import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, AsyncStorage, Alert } from 'react-native';
import { Scene, Router, Drawer, ActionConst } from 'react-native-router-flux';
import { connect } from 'react-redux';
import SideMenuContent from './components/SideMenu';
import Login from './components/Login';
import Pickups from './components/Pickups';
import Deliveries from './components/Deliveries';
import PickupsComplete from './components/PickupsComplete';
import DeliveriesComplete from './components/DeliveriesComplete';
import Baskets from './components/Baskets';
import Services from './components/Services';
import Modal from './components/Modal';

const MENU_ICON = require('./Assets/Menu/Hamburger.png');

const deviceWidth = Dimensions.get('window').width;

class RouterComponent extends Component {

  constructor() {
    super();
      this.state = { hasToken: false, isLoaded: false };
}

async componentWillMount() {
   await AsyncStorage.getItem('token').then((token) => {
    this.setState({ hasToken: token !== null, isLoaded: true });
  });
}


  render() {
    if (!this.state.isLoaded) {
      return (
        <ActivityIndicator />
      );
    }
    return (

    <Router
         leftButtonIconStyle={{ height: 20, width: 20, resizeMode: 'contain', tintColor: '#FFFFFF' }}
         navigationBarStyle={{ backgroundColor: '#1E88E5', borderBottomWidth: 0 }}
         titleStyle={{ color: '#FFF', width: deviceWidth }}
    >

  <Modal hideNavBar>
    <Scene key="root">
      <Scene key="login" component={Login} hideNavBar initial={!this.state.hasToken} />
      <Drawer
                hideNavBar
                initial={this.state.hasToken}
                key="drawer"
                contentComponent={SideMenuContent}
                drawerImage={MENU_ICON}
                drawerWidth={250}
      >
      <Scene key="pickup" title="Pickups" component={Pickups} />
      <Scene key="delivery" title="Deliveries" component={Deliveries} />
      <Scene key="pickup_complete" title="Completed Pickups" component={PickupsComplete} />
      <Scene key="del_complete" title="Completed Deliveries" component={DeliveriesComplete} />
      </Drawer>
      <Scene key="baskets" title="Baskets" component={Baskets} />
      <Scene key="services" component={Services} />
    </Scene>
    <Scene key="modal" component={Modal} hideNavBar />
  </Modal>

    </Router>
  );
  }
}

const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  return { session_token };
};

export default connect(mapStateToProps)(RouterComponent);
