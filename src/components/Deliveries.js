import React, { Component } from 'react';
import { View, Text, ListView, Image, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { phonecall, text } from 'react-native-communications';
import getDirections from 'react-native-google-maps-directions';
import { deliveryFetch, deliveryComplete } from '../actions';
import { CardSection, Button } from './common';

const HOME_ICON = require('../Assets/Homepage/icn_home.png');
const WORK_ICON = require('../Assets/Homepage/icn_office.png');
const OTHER_ICON = require('../Assets/Homepage/icn_other.png');

const CALL_ICON = require('../Assets/Menu/icn__0006_callus.png');
const SMS_ICON = require('../Assets/Menu/sms.png');
const MAP_ICON = require('../Assets/Menu/map.png');

class Deliveries extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

async componentWillMount() {
  await AsyncStorage.getItem('token').then((token) => {
    this.setState({ token });
  });
    this.props.deliveryFetch(this.props.session_token || this.state.token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.status.d_status === true &&
      this.props.status.d_status !== nextProps.status.d_status) {
        this.props.deliveryFetch(this.props.session_token || this.state.token);
      }

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.list)
      });
  }

  onCompleteBtnPress(orderID) {
    Alert.alert(
  '',
  'Are you sure?',
    [
      { text: 'Cancel' },
      { text: 'Confirm',
      onPress: () => this.props.deliveryComplete(orderID, this.props.session_token
        || this.state.token) },
    ]
  );
  }

  handleGetDirections(lat, lng) {
      const data = {
        destination: {
          latitude: Number(lat),
          longitude: Number(lng)
        },
        params: [
          {
            key: 'travelmode',
            value: 'driving'
          },
          {
            key: 'dir_action',
            value: 'navigate'
          }
        ]
      };

      getDirections(data);
    }

  renderCompleteBtn(id) {
    return (
    <Button
      onPress={() => this.onCompleteBtnPress(id)}
    >
      COMPLETE
    </Button>
  );
  }

  renderAddressIcon(tag) {
      if (tag === 'Home') {
        return (
        <Image source={HOME_ICON} style={styles.AddressIconStyle} />
      );
    } else if (tag === 'Work') {
      return (
      <Image source={WORK_ICON} style={styles.AddressIconStyle} />
    );
      }
      return (
      <Image source={OTHER_ICON} style={styles.AddressIconStyle} />
    );
  }

  render() {
    if (!this.state.dataSource.getRowCount()) {
      return (
        <Text style={styles.NoOrderTextStyle}>No Deliveries Assigned</Text>
      );
    }
    return (
      <View style={{ flex: 1 }}>
      <ListView
        dataSource={this.state.dataSource}
        renderRow={(data) =>
          <View style={styles.ViewStyle}>
            <Text style={styles.TitleTextStyle}>ORDER ID: {data.id}</Text>
            <CardSection style={styles.CustomerSectionStyle}>
            <Text style={styles.HeadTextStyle}>{data.customer.name.toUpperCase()}</Text>
            <TouchableOpacity
              onPress={() => { phonecall(`+91${data.customer.mobile}`, true); }}
            >
              <Image
                source={CALL_ICON}
                style={styles.IconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { text(`+91${data.customer.mobile}`, ''); }}
            >
              <Image
                source={SMS_ICON}
                style={styles.IconStyle}
              />

            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.handleGetDirections(data.address.lat, data.address.lng); }}
            >
              <Image
                source={MAP_ICON}
                style={styles.IconStyle}
              />

            </TouchableOpacity>
            </CardSection>

            <CardSection style={styles.DeliverySectionStyle}>
            <Text style={styles.DeliveryTextStyle}>{data.pickup_date.split(',')[0]},
              {data.pickup_date.split(',')[1]} </Text>
            <Text style={styles.DeliveryTextStyle}> {data.pickup_time}</Text>
            </CardSection>

            <CardSection >
              {this.renderAddressIcon(data.address.tag)}
              <View style={styles.AddressViewStyle}>
              <Text style={styles.TextStyle}>{data.address.flat.toUpperCase()}</Text>
              <Text style={styles.TextStyle}>{data.address.street.toUpperCase()}</Text>
              <Text style={styles.TextStyle}>AREA:
                  <Text style={styles.HeadTextStyle}>  {data.address.landmark.toUpperCase()}</Text>
              </Text>

              </View>
              </CardSection>
              <Text style={styles.AmountTextStyle}>Payment Status:
              <Text style={{ color: '#1976d2' }}> {data.payment.payment_status}</Text> Amount:
              <Text style={{ color: '#1976d2' }}> {data.payment.total_amount}</Text>
              </Text>
              {this.renderCompleteBtn(data.id)}

          </View>
        }
      />
      </View>

    );
  }
}

const styles = {
  ViewStyle: {
    paddingVertical: 15,
    borderWidth: 1,
    borderBottomWidth: 2,
    borderRadius: 2,
    borderColor: '#000',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    marginHorizontal: 5,
    marginTop: 10
  },
  CustomerSectionStyle: {
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  TextStyle: {
    fontSize: 15,
    fontWeight: '400'
  },
  HeadTextStyle: {
    fontSize: 16,
    fontWeight: '500'
  },
  AmountTextStyle: {
    fontSize: 16,
    marginLeft: 20,
    fontWeight: '500',
    marginBottom: 10,
  },
  AddressIconStyle: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    paddingHorizontal: 30
  },
  IconStyle: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  AddressViewStyle: {
    paddingRight: 20,
    marginRight: 20
  },
  TitleTextStyle: {
    fontSize: 16,
    marginLeft: 20
  },
  DeliverySectionStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  DeliveryTextStyle: {
    fontSize: 16
  },
  NoOrderTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    marginTop: 30
  }


};

const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  const list = state.list;
  const status = state.status;
  return { list, status, session_token };
};

export default connect(mapStateToProps, { deliveryFetch, deliveryComplete })(Deliveries);
