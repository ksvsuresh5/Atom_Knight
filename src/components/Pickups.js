import React, { Component } from 'react';
import { View, Text, ListView, Image, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { phonecall, text } from 'react-native-communications';
import getDirections from 'react-native-google-maps-directions';
import { pickupFetch, pickupComplete, basketFetch } from '../actions';
import { CardSection, Button } from './common';

const HOME_ICON = require('../Assets/Homepage/icn_home.png');
const WORK_ICON = require('../Assets/Homepage/icn_office.png');
const OTHER_ICON = require('../Assets/Homepage/icn_other.png');

const CALL_ICON = require('../Assets/Menu/icn__0006_callus.png');
const SMS_ICON = require('../Assets/Menu/sms.png');
const MAP_ICON = require('../Assets/Menu/map.png');

class Pickups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

async componentWillMount() {
  await AsyncStorage.getItem('token').then((token) => {
  this.setState({ token });
});
    this.props.pickupFetch(this.props.session_token || this.state.token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.status.p_status === true) {
        this.props.pickupFetch(this.props.session_token || this.state.token);
    }

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.list)
      });
  }

  onCompleteBtnPress(orderID) {
    Alert.alert(
  'Payment Complete?',
  'Please make sure the payment is complete',
    [
      { text: 'Cancel' },
      { text: 'Confirm',
      onPress: () => this.props.pickupComplete(orderID,
        this.props.session_token || this.state.token) },
    ]
  );
}

handleGetDirections(lat, lng) {
    const data = {
      destination: {
        latitude: Number(lat),
        longitude: Number(lng)
      },
      params: [
        {
          key: 'travelmode',
          value: 'driving'
        },
        {
          key: 'dir_action',
          value: 'navigate'
        }
      ]
    };

    getDirections(data);
  }

  renderAddressIcon(tag) {
    if (tag === 'Home') {
        return (
        <Image source={HOME_ICON} style={styles.AddressIconStyle} />
      );
    } else if (tag === 'Work') {
      return (
      <Image source={WORK_ICON} style={styles.AddressIconStyle} />
    );
      }
      return (
      <Image source={OTHER_ICON} style={styles.AddressIconStyle} />
    );
  }

  renderButton(taskStatus, id) {
    if (taskStatus === 'inprogress') {
      return (
        <TouchableOpacity
          style={styles.ButtonStyle} onPress={() =>
          this.props.basketFetch(id, this.props.session_token || this.state.token, 'edit')}
        >
          <Text style={styles.ButtonTextStyle}>
            EDIT BASKET
          </Text>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        style={styles.ButtonStyle} onPress={() =>
        Actions.baskets({ order_id: id })}
      >
        <Text style={styles.ButtonTextStyle}>
          ADD BASKET
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    if (!this.state.dataSource.getRowCount()) {
      return (
        <Text style={styles.NoOrderTextStyle}>No Pickups Assigned</Text>
      );
    }
    return (
      <View style={{ flex: 1 }}>
      <ListView
        dataSource={this.state.dataSource}
        renderRow={(data) =>
          <View style={styles.ViewStyle}>
            <Text style={styles.TitleTextStyle}>ORDER ID: {data.id}</Text>
            <CardSection style={styles.CustomerSectionStyle}>
            <Text style={styles.HeadTextStyle}>{data.customer.name.toUpperCase()}</Text>
            <TouchableOpacity
              onPress={() => { phonecall(`+91${data.customer.mobile}`, true); }}
            >
              <Image
                source={CALL_ICON}
                style={styles.IconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { text(`+91${data.customer.mobile}`, ''); }}
            >
              <Image
                source={SMS_ICON}
                style={styles.IconStyle}
              />

            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.handleGetDirections(data.address.lat, data.address.lng); }}
            >
              <Image
                source={MAP_ICON}
                style={styles.IconStyle}
              />

            </TouchableOpacity>
            </CardSection>

            <CardSection style={styles.PickupSectionStyle}>
            <Text style={styles.PickupTextStyle}>{data.pickup_date.split(',')[0]},
              {data.pickup_date.split(',')[1]} </Text>
            <Text style={styles.PickupTextStyle}> {data.pickup_time}</Text>
            </CardSection>

            <CardSection >
              {this.renderAddressIcon(data.address.tag)}
              <View style={styles.AddressViewStyle}>
              <Text style={styles.TextStyle}>{data.address.flat.toUpperCase()}</Text>
              <Text style={styles.TextStyle}>{data.address.street.toUpperCase()}</Text>
              <Text style={styles.TextStyle}>AREA:
                  <Text style={styles.HeadTextStyle}>  {data.address.landmark.toUpperCase()}</Text>
              </Text>
              </View>
              </CardSection>
              {this.renderButton(data.task_status, data.id)}
              <Button
                onPress={() => this.onCompleteBtnPress(data.id)}
              >
                COMPLETE
              </Button>

          </View>

        }
      />
      </View>

    );
  }
}

const styles = {
  ViewStyle: {
    paddingVertical: 15,
    borderWidth: 1,
    borderBottomWidth: 2,
    borderRadius: 2,
    borderColor: '#000',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    marginHorizontal: 5,
    marginTop: 10
  },
  CustomerSectionStyle: {
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  TextStyle: {
    fontSize: 15,
    fontWeight: '400'
  },
  HeadTextStyle: {
    fontSize: 16,
    fontWeight: '500'
  },
  TitleTextStyle: {
    fontSize: 16,
    marginLeft: 20
  },
  PickupTextStyle: {
    fontSize: 16
  },
  AddressIconStyle: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    paddingHorizontal: 30
  },
  IconStyle: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  ButtonTextStyle: {
    alignSelf: 'center',
    color: '#1E88E5',
    fontSize: 16,
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10
  },
  ButtonStyle: {
    flex: 0.5,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#007aff',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  AddressViewStyle: {
    paddingRight: 20,
    marginRight: 20
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center'
  },
  PickupSectionStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  NoOrderTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    marginTop: 30
  }

};

const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  const list = state.list;
  const status = state.status;
  return { session_token, list, status };
};

export default connect(mapStateToProps, { pickupFetch, pickupComplete, basketFetch })(Pickups);
