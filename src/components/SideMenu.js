import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

const PICKUP_ICON = require('../Assets/Menu/pickup_inactive.png');
const PICKUPCOMPLETE_ICON = require('../Assets/Menu/pickup.png');
const DEL_ICON = require('../Assets/Menu/delivery.png');
const DELCOMPLETE_ICON = require('../Assets/Menu/delivery_active.png');


class SideMenu extends Component {

	constructor(props) {
    super(props);
    this.state = {
      profile: [],
			name: '',
    };
  }

async	componentWillMount() {
	await AsyncStorage.getItem('name').then((name) => {
	this.setState({ name });
});
	}

	onLogoutAlert() {
		Alert.alert(
  '',
  'Sure you want to Logout?',
    [
      { text: 'Cancel' },
      { text: 'Confirm',
      onPress: () => this.onLogoutPress() }
    ]
  );
	}

	async onLogoutPress() {
		await AsyncStorage.clear();
		Actions.login();
	}

	render() {
		const { drawer } = this.context;
		return (
			<View style={styles.sidemenumaindiv}>
				<View style={styles.nameViewStyle}>
				<Text style={styles.nameStyle}>
					{this.state.name}
				</Text>
				</View>
				<TouchableOpacity
					onPress={() => { Actions.pickup(); }}
					style={styles.sidemenuitems}
				>
					<Image
						source={PICKUP_ICON}
						style={styles.menuImage}
					/>
					<Text style={styles.sidemenuText}>{'Pickups'}</Text>
				</TouchableOpacity>

				<TouchableOpacity
					onPress={() => { Actions.delivery(); }}
					style={styles.sidemenuitems}
				>
					<Image
						source={DEL_ICON}
						style={styles.menuImage}
					/>
					<Text style={styles.sidemenuText}>{'Deliveries'}</Text>
				</TouchableOpacity>

				<TouchableOpacity
					onPress={() => { Actions.pickup_complete(); }}
					style={styles.sidemenuitems}
				>
					<Image
						source={PICKUPCOMPLETE_ICON}
						style={styles.menuImage}
					/>
					<Text style={styles.sidemenuText}>{'Completed Pickups'}</Text>
				</TouchableOpacity>

				<TouchableOpacity
					onPress={() => { Actions.del_complete(); }}
					style={styles.sidemenuitems}
				>
					<Image
						source={DELCOMPLETE_ICON}
						style={styles.menuImage}
					/>
					<Text style={styles.sidemenuText}>{'Completed Deliveries'}</Text>
				</TouchableOpacity>

				<TouchableOpacity
					onPress={() => this.onLogoutAlert()}
					style={styles.logoutViewStyle}
				>
					<Text style={styles.logoutText}>{'LOGOUT'}</Text>
				</TouchableOpacity>

			</View>
		);
	}
}

const styles = {
	sidemenuitems: {
      padding: 10,
      paddingBottom: 0,
			paddingLeft: 20,
      height: 50,
      flexDirection: 'row',
      alignItems: 'center'
  },
  sidemenuText: {
      fontSize: 16,
      textAlign: 'left',
      color: '#666',
      fontWeight: '400',
			paddingLeft: 20,
      marginLeft: 10
  },
  sidemenumaindiv: {
      flex: 1,
      flexDirection: 'column',

  },
	menuImage: {
		height: 30,
		width: 30,
		resizeMode: 'contain'
	},
	nameStyle: {
		fontSize: 20,
		textAlign: 'left',
		color: '#f9f9f9',
		fontWeight: '400',
		marginLeft: '30%',
		paddingTop: '5%',

	},
	nameViewStyle: {
		backgroundColor: '#1976d2',
		alignItems: 'flex-start',
		height: '10%',
	},
	logoutViewStyle: {
		padding: 10,
		marginTop: '7%',
		height: '10%',
		flexDirection: 'row',
		alignItems: 'center',
		borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
	},
	logoutText: {
		fontSize: 17,
		textAlign: 'left',
		color: '#1976d2',
		fontWeight: '400',
		marginLeft: '30%',
		marginTop: '10%'
	}
};

export default (SideMenu);
