import React, { Component } from 'react';
import { ScrollView, View, Text, ListView,
  AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import NumericInput, { calcSize } from 'react-native-numeric-input';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import { serviceFetch, countChanged } from '../actions';
import { CardSection, Panel, Button } from './common';

let regularUpperData = [];
let regularLowerData = [];
let regularOtherData = [];
let householdData = [];
let swrBasket = [];
let sirBasket = [];
let swhBasket = [];
let sihBasket = [];
let dcBasket = [];
let pmBasket = [];

let serviceType = '';

class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: [],
      regularupperdataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      regularlowerdataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      regularotherdataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      householddataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

async componentWillMount() {
  await AsyncStorage.getItem('token').then((token) => {
  this.setState({ token });
});
    if (this.props.service_type === 'standard_wash_and_fold') {
      this.props.serviceFetch('swfold', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Wash and Fold' });
        swrBasket = this.props.data.swrBasket;
    } else if (this.props.service_type === 'household_wash_and_fold') {
      this.props.serviceFetch('hhwfold', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Wash and Fold Household' });
        swhBasket = this.props.data.swhBasket;
    } else if (this.props.service_type === 'standard_wash_and_iron') {
      this.props.serviceFetch('swiron', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Wash and Iron' });
        sirBasket = this.props.data.sirBasket;
    } else if (this.props.service_type === 'household_wash_and_iron') {
      this.props.serviceFetch('hhwiron', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Wash and Iron Household' });
        sihBasket = this.props.data.sihBasket;
    } else if (this.props.service_type === 'premium') {
      this.props.serviceFetch('premium', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Premium' });
        pmBasket = this.props.data.pmBasket;
    } else if (this.props.service_type === 'dry_cleaning') {
      this.props.serviceFetch('drycleaning', this.props.session_token || this.state.token);
        Actions.refresh({ title: 'Dry Cleaning' });
        dcBasket = this.props.data.dcBasket;
    }
  }

  componentWillReceiveProps(nextProps) {
    serviceType = nextProps.service_type;
    this.setState({ service_type: serviceType });
    regularUpperData = [];
    regularLowerData = [];
    regularOtherData = [];
    householdData = [];
    if (nextProps.services) {
      for (let i = 0; i < nextProps.services.length; i++) {
        if (nextProps.services[i].service_type === serviceType && nextProps.services[i].upper
        && nextProps.services[i].category === 0 && nextProps.services[i].active) {
          regularUpperData.push(nextProps.services[i]);
        }
      }
      this.setState({
        regularupperdataSource: this.state.regularupperdataSource.cloneWithRows(regularUpperData)
      });

      for (let i = 0; i < nextProps.services.length; i++) {
        if (nextProps.services[i].service_type === serviceType &&
          nextProps.services[i].upper === false &&
          nextProps.services[i].category === 0 && nextProps.services[i].active) {
          regularLowerData.push(nextProps.services[i]);
        }
      }
      this.setState({
        regularlowerdataSource: this.state.regularlowerdataSource.cloneWithRows(regularLowerData)
      });

      for (let i = 0; i < nextProps.services.length; i++) {
        if (nextProps.services[i].service_type === serviceType && !nextProps.services[i].upper &&
        nextProps.services[i].upper !== false && nextProps.services[i].category === 0 &&
        nextProps.services[i].active) {
          regularOtherData.push(nextProps.services[i]);
        }
      }
      this.setState({
        regularotherdataSource: this.state.regularotherdataSource.cloneWithRows(regularOtherData)
      });

      for (let i = 0; i < nextProps.services.length; i++) {
          if (nextProps.services[i].service_type === serviceType &&
            nextProps.services[i].category === 1 && nextProps.services[i].active) {
            householdData.push(nextProps.services[i]);
          }
        }
        this.setState({
          householddataSource: this.state.householddataSource.cloneWithRows(householdData) });
    }
  }

  onQtyChange(value, id, stype, price) {
    let sType = '';
    if (stype === 'standard_wash_and_fold') {
      sType = 'wf';
    } else if (stype === 'household_wash_and_fold') {
      sType = 'wfh';
    } else if (stype === 'standard_wash_and_iron') {
      sType = 'wi';
    } else if (stype === 'household_wash_and_iron') {
      sType = 'wih';
    } else if (stype === 'premium') {
      sType = 'pm';
    } else if (stype === 'dry_cleaning') {
      sType = 'dc';
    }
      this.props.countChanged({
       prop: [`${sType}regular${id}`], value: [value, id, price] });
  }

  onHHQtyChange(value, id, stype, price) {
    let sType = '';
    if (stype === 'premium') {
      sType = 'pm';
    } else if (stype === 'dry_cleaning') {
      sType = 'dc';
    }
      this.props.countChanged({
       prop: [`${sType}hh{id}`], value: [value, id, price] });
  }


  onProceedBtnPress() {
    const qty = this.props.item;
    const data = this.props.data;

    const swRegularArr = Object.keys(qty).map(key => key.indexOf('wfregular') > -1 &&
    qty[key]).filter(val => !!val);
    for (let i = 0; i < swRegularArr.length; i++) {
      const obj = _.find(data.swrBasket, { service_id: swRegularArr[i][1] });
      if (obj === undefined) {
      data.swrBasket.push({ id: null,
        service_id: swRegularArr[i][1],
        quantity: parseInt(swRegularArr[i][0], 10),
         });
       } else {
         obj.quantity = parseInt(swRegularArr[i][0], 10);
       }
    }

    const swHHArr = Object.keys(qty).map(key => key.indexOf('wfhregular') > -1 &&
    qty[key]).filter(val => !!val);
    for (let i = 0; i < swHHArr.length; i++) {
      const obj = _.find(data.swhBasket, { service_id: swHHArr[i][1] });
      if (obj === undefined) {
        data.swhBasket.push({ id: null,
          service_id: swHHArr[i][1],
          quantity: parseInt(swHHArr[i][0], 10), });
      } else {
        obj.quantity = parseInt(swHHArr[i][0], 10);
      }
    }

    const siRegularArr = Object.keys(qty).map(key => key.indexOf('wiregular') > -1 &&
    qty[key]).filter(val => !!val);
    for (let i = 0; i < siRegularArr.length; i++) {
      const obj = _.find(data.sirBasket, { service_id: siRegularArr[i][1] });
      if (obj === undefined) {
      data.sirBasket.push({ service_id: siRegularArr[i][1],
        quantity: parseInt(siRegularArr[i][0], 10) });
      } else {
        obj.quantity = parseInt(siRegularArr[i][0], 10);
      }
    }

    const siHHArr = Object.keys(qty).map(key => key.indexOf('wihregular') > -1 &&
    qty[key]).filter(val => !!val);
    for (let i = 0; i < siHHArr.length; i++) {
      const obj = _.find(data.sihBasket, { service_id: siHHArr[i][1] });
      if (obj === undefined) {
      data.sihBasket.push({ service_id: siHHArr[i][1],
        quantity: parseInt(siHHArr[i][0], 10) });
      } else {
        obj.quantity = parseInt(siHHArr[i][0], 10);
      }
    }

    const dcArr = Object.keys(qty).map(key => key.indexOf('dc') > -1 &&
    qty[key]).filter(val => !!val);
    data.dcBasket.cost = 0;
    for (let i = 0; i < dcArr.length; i++) {
      const obj = _.find(data.dcBasket, { service_id: dcArr[i][1] });
      if (obj === undefined) {
        data.dcBasket.push({ service_id: dcArr[i][1],
        quantity: parseInt(dcArr[i][0], 10) });
      } else {
        obj.quantity = parseInt(dcArr[i][0], 10);
      }
      data.dcBasket.cost += parseInt(dcArr[i][0], 10) * parseInt(dcArr[i][2], 10);
    }

    const pmArr = Object.keys(qty).map(key => key.indexOf('pm') > -1 &&
    qty[key]).filter(val => !!val);
    data.pmBasket.cost = 0;
    for (let i = 0; i < pmArr.length; i++) {
      const obj = _.find(data.pmBasket, { service_id: pmArr[i][1] });
      if (obj === undefined) {
      data.pmBasket.push({ service_id: pmArr[i][1],
        quantity: parseInt(pmArr[i][0], 10) });
      } else {
        obj.quantity = parseInt(pmArr[i][0], 10);
      }
      data.pmBasket.cost += parseInt(pmArr[i][0], 10) * parseInt(pmArr[i][2], 10);
    }

    if (this.state.service_type === 'standard_wash_and_fold' ||
    this.state.service_type === 'standard_wash_and_iron' ||
    this.state.service_type === 'household_wash_and_fold' ||
    this.state.service_type === 'household_wash_and_iron') {
      Actions.modal({ service_type: this.state.service_type, qty, data });
    } else if (this.state.service_type === 'premium' ||
    this.state.service_type === 'dry_cleaning') {
      Actions.baskets({ qty, serviceType: this.state.service_type, data });
  }
}

renderNumberInput(data, type) {
  let basket = '';
  if (data.service_type === 'standard_wash_and_fold') {
    basket = swrBasket;
  } else if (data.service_type === 'standard_wash_and_iron') {
    basket = sirBasket;
  } else if (data.service_type === 'household_wash_and_fold') {
    basket = swhBasket;
  } else if (data.service_type === 'household_wash_and_iron') {
    basket = sihBasket;
  } else if (data.service_type === 'dry_cleaning') {
    basket = dcBasket;
  } else if (data.service_type === 'premium') {
    basket = pmBasket;
  }

  const serviceObj = _.find(basket, { service_id: data.id });

  if (type === 'household') {
    return (
      <NumericInput
        value={this.props.item[`${data.service_type}hh${data.id}`]}
        initValue={serviceObj ? serviceObj.quantity : 0}
        onChange={(value) =>
          this.onQtyChange(value, data.id, data.service_type, data.price)}
        totalWidth={calcSize(240)}
        totalHeight={calcSize(65)}
        iconSize={calcSize(25)}
        inputStyle={{ fontSize: 16, color: '#1976D2' }}
        minValue={0}
        rounded
        textColor='#B0228C'
        iconStyle={{ color: '#1976D2' }}
      />
    );
  }
  return (
    <NumericInput
      value={this.props.item[`${data.service_type}regular${data.id}`]}
      initValue={serviceObj ? serviceObj.quantity : 0}
      onChange={(value) =>
        this.onQtyChange(value, data.id, data.service_type, data.price)}
      totalWidth={calcSize(240)}
      totalHeight={calcSize(65)}
      iconSize={calcSize(25)}
      inputStyle={{ fontSize: 15, color: '#1976D2' }}
      minValue={0}
      rounded
      textColor='#B0228C'
      iconStyle={{ color: '#1976D2' }}
    />
  );
}

renderServices() {
  if (this.state.service_type === 'standard_wash_and_fold' ||
  this.state.service_type === 'standard_wash_and_iron') {
    return (
      <ScrollableTabView
        tabBarBackgroundColor='#ddd'
        tabBarActiveTextColor='#1976d2'
        tabBarInactiveTextColor='#rgb(82, 84, 83)'
        tabBarUnderlineStyle={{ backgroundColor: '#ffc107', height: 2 }}
      >
        <ScrollView tabLabel='Upper'>
          <View>
            <ListView
              dataSource={this.state.regularupperdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

        <ScrollView tabLabel='Lower'>
          <View>
            <ListView
              dataSource={this.state.regularlowerdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

        <ScrollView tabLabel='Other'>
          <View>
            <ListView
              dataSource={this.state.regularotherdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>


  </ScrollableTabView>
    );
  } else if (this.state.service_type === 'household_wash_and_fold' ||
  this.state.service_type === 'household_wash_and_iron') {
    return (
      <ScrollView >
        <View>
          <ListView
            dataSource={this.state.householddataSource}
            renderRow={(data) =>
              <View>
                <CardSection style={styles.CardSectionStyle}>
                  <Text style={styles.TextStyle}>{data.cloth}</Text>
                  <View style={styles.InputContainerStyle}>
                    {this.renderNumberInput(data, 'regular')}
                  </View>
                </CardSection>
              </View>
            }
          />
        </View>
      </ScrollView>
    );
  } else if (this.state.service_type === 'premium') {
    return (
      <ScrollableTabView
         tabBarBackgroundColor='#ddd'
         tabBarActiveTextColor='#1976d2'
         tabBarInactiveTextColor='#rgb(82, 84, 83)'
         tabBarUnderlineStyle={{ backgroundColor: '#ffc107', height: 2 }}
      >
        <ScrollView tabLabel='Upper'>
          <View>
            <ListView
              dataSource={this.state.regularupperdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

        <ScrollView tabLabel='Lower'>
          <View>
            <ListView
              dataSource={this.state.regularlowerdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

   </ScrollableTabView>

    );
  } else if (this.state.service_type === 'dry_cleaning') {
    return (
      <ScrollableTabView
         tabBarBackgroundColor='#ddd'
         tabBarActiveTextColor='#1976d2'
         tabBarInactiveTextColor='#rgb(82, 84, 83)'
         tabBarUnderlineStyle={{ backgroundColor: '#ffc107', height: 2 }}
      >
        <ScrollView tabLabel='Upper'>
          <View>
            <ListView
              dataSource={this.state.regularupperdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

        <ScrollView tabLabel='Lower'>
          <View>
            <ListView
              dataSource={this.state.regularlowerdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

        <ScrollView tabLabel='Other'>
          <View>
            <ListView
              dataSource={this.state.regularotherdataSource}
              renderRow={(data) =>
                <View>
                  <CardSection style={styles.CardSectionStyle}>
                    <Text style={styles.TextStyle}>{data.cloth}</Text>
                    <View style={styles.InputContainerStyle}>
                      {this.renderNumberInput(data, 'regular')}
                    </View>
                  </CardSection>
                </View>
              }
            />
          </View>
        </ScrollView>

         <ScrollView tabLabel='Household'>
           <ListView
             dataSource={this.state.householddataSource}
             renderRow={(data) =>
               <View >
                 <CardSection style={styles.CardSectionStyle}>
                   <Text style={styles.TextStyle}>{data.cloth}</Text>
                   <View style={styles.InputContainerStyle}>
                     {this.renderNumberInput(data, 'household')}
                   </View>
                 </CardSection>
               </View>
             }
           />

         </ScrollView>
   </ScrollableTabView>

    );
  }
}

render() {
     return (
       <View style={{ flex: 1, flexDirection: 'column' }} >
      {this.renderServices()}

   <View style={styles.SubmitButtonViewStyle}>
   <Button
     onPress={() => this.onProceedBtnPress()}
   >
     PROCEED
   </Button>
   </View>
 </View>


     );
 }
}

const styles = {
  CardSectionStyle: {
    justifyContent: 'space-between',
    borderColor: '#ddd',
    borderBottomWidth: 1,
    alignItems: 'center'
  },
  TextStyle: {
    fontWeight: '300',
    fontSize: 14,
    width: '50%'
  },
  PickerStyle: {
    width: 50,
    height: 25,
  },
  PickerCardSectionStyle: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  SubmitButtonViewStyle: {
    height: 50,
    marginBottom: 5
  },
  InputStyle: {
    width: 50,
    height: 40,
    fontSize: 14,
    textAlign: 'center'
  },
  InputContainerStyle: {
    marginLeft: 20,
  },
};

const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  const services = state.services;
  const item = state.item;
  return { services, item, session_token };
};


export default connect(mapStateToProps, { serviceFetch, countChanged })(Services);
