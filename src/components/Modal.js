import React, { Component } from 'react';
import { View, Text, Animated, Dimensions, TextInput, TouchableOpacity, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { weightChanged } from '../actions';
import { CardSection } from './common';

const { height: deviceHeight } = Dimensions.get('window');
const deviceWidth = Dimensions.get('window').width;

const CLOSE_ICON = require('../Assets/Menu/close.png');

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offset: new Animated.Value(-deviceHeight)
    };
  }

  componentWillMount() {
    console.log('mod');
    console.log(this.props);
    if (this.props.service_type === 'standard_wash_and_fold') {
      this.setState({ service_type: 'Wash and Fold', stype: 'wf' });
    } else if (this.props.service_type === 'household_wash_and_fold') {
      this.setState({ service_type: 'Household Wash and Fold', stype: 'wfh' });
    } else if (this.props.service_type === 'standard_wash_and_iron') {
      this.setState({ service_type: 'Wash and Iron', stype: 'wi' });
    } else if (this.props.service_type === 'household_wash_and_iron') {
      this.setState({ service_type: 'Household Wash and Iron', stype: 'wih' });
    }
  }

  componentDidMount() {
    Animated.timing(this.state.offset, {
      duration: 150,
      toValue: 0
    }).start();
  }

  onNextBtnPress() {
    const qty = this.props.qty;
    const weight = this.props.weight;
    const serviceType = this.props.service_type;
    const data = this.props.data;
    Actions.baskets({ qty, weight, serviceType, data });
  }

  closeModal() {
    Animated.timing(this.state.offset, {
      duration: 150,
      toValue: -deviceHeight
    }).start(Actions.pop);
  }

  render() {
    return (
      <Animated.View
style={[styles.container, { backgroundColor: 'rgba(52,52,52,0.5)' },
      { transform: [{ translateY: this.state.offset }] }]}
      >
        <View
style={{
          width: deviceWidth * 0.8,
          height: 250,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white'
        }}
        >
          <CardSection>
          <Text style={styles.TitleStyle}>{this.state.service_type}</Text>
          <TouchableOpacity style={styles.CloseButtonStyle} onPress={this.closeModal.bind(this)}>
            <Image
              source={CLOSE_ICON}
              style={styles.IconStyle}
            />
          </TouchableOpacity>
          </CardSection>
          <CardSection style={styles.SectionStyle}>
            <Text style={styles.labelStyle}>Weight: </Text>
            <View style={styles.InputContainerStyle}>
            <TextInput
              label="Weight"
              keyboardType='phone-pad'
              autoCorrect={false}
              selectionColor='#fff'
              style={styles.InputStyle}
              onChangeText={(value) => {
                this.props.weightChanged({
                 prop: `${this.state.stype}rw`, value });
              }}
              value={this.props.weight[`${this.state.stype}rw`]}
            />
            </View>
            <Text style={styles.labelStyle}>kg</Text>
          </CardSection>

          <TouchableOpacity style={styles.ButtonStyle} onPress={() => this.onNextBtnPress()}>
            <Text style={styles.ButtonTextStyle}>
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  InputStyle: {
    width: 50,
    height: 50,
    fontSize: 16,
    textAlign: 'center'
  },
  InputContainerStyle: {
    marginHorizontal: 10,
  },
  labelStyle: {
    fontSize: 18,
    marginTop: 15
  },
  CloseButtonStyle: {
    alignSelf: 'center',
    marginLeft: deviceWidth / 3
  },
  ButtonTextStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  ButtonStyle: {
    alignSelf: 'stretch',
    backgroundColor: '#1976d2',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5,
    shadowColor: '#000',
     shadowOffset: {
       width: 0,
       height: 2
     },
     shadowOpacity: 0.2,
     marginTop: 20
  },
  IconStyle: {
    height: 20,
    width: 20
  },
  TitleStyle: {
    fontSize: 20,
    color: '#1976d2',
  }
};

const mapStateToProps = (state) => {
  const weight = state.weight;
  return { weight };
};

export default connect(mapStateToProps, { weightChanged })(Modal);
