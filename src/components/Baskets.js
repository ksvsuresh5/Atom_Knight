import React, { Component } from 'react';
import { ScrollView, View, Text, Dimensions, Alert,
  ActivityIndicator, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { CardSection, Button } from './common';
import { createBasket, updateBasket } from '../actions';

const deviceWidth = Dimensions.get('window').width;

const data = {
  wfrw: 0,
  wfhw: 0,
  wirw: 0,
  wihw: 0,
  wfrq: 0,
  wfhq: 0,
  wirq: 0,
  wihq: 0,
  dcq: 0,
  pmq: 0,
  order_id: 0,
  swrBasket: [],
  sirBasket: [],
  swhBasket: [],
  sihBasket: [],
  dcBasket: [],
  pmBasket: [],
  state: ''
};

class Baskets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }

 componentWillMount() {
   if (this.props.state) {
     data.state = this.props.state;
   }
  AsyncStorage.getItem('token').then((token) => {
     this.setState({ token });
   });

   if (this.props.sb) {
     this.props.sb.forEach((x) => {
      if (x.service_type === 'standard_wash_and_fold') {
        data.wfrq = x.regular.quantity;
        data.wfrw = x.regular.weight;
        data.swrBasket = x.regular.baskets;
        data.swrBasket.id = x.regular.id;
      }
      if (x.service_type === 'household_wash_and_fold') {
        data.wfhq = x.regular.quantity;
        data.wfhw = x.regular.weight;
        data.swhBasket = x.regular.baskets;
        data.swhBasket.id = x.regular.id;
      }
      if (x.service_type === 'standard_wash_and_iron') {
        data.wirq = x.regular.quantity;
        data.wirw = x.regular.weight;
        data.sirBasket = x.regular.baskets;
        data.sirBasket.id = x.regular.id;
      }
      if (x.service_type === 'household_wash_and_iron') {
        data.wihq = x.regular.quantity;
        data.wihw = x.regular.weight;
        data.sihBasket = x.regular.baskets;
        data.sihBasket.id = x.regular.id;
      }
      if (x.service_type === 'premium') {
        data.pmq = x.regular.quantity;
        data.pmBasket = x.regular.baskets;
        data.pmBasket.id = x.regular.id;
      }
      if (x.service_type === 'dry_cleaning') {
        data.dcq = x.regular.quantity;
        data.dcBasket = x.regular.baskets;
        data.dcBasket.id = x.regular.id;
      }
  });
}

    data.order_id = this.props.order_id || this.props.data.order_id;
    if (this.props.data) {
      this.setState({ data: this.props.data });
    } else if (this.props.sb && this.props.sb.length > 0) {
      this.setState({ data });
    }
    this.setState({ order_id: this.props.order_id });


    if (this.props.serviceType === 'standard_wash_and_fold') {
        data.wfrw = this.props.weight.wfrw;
        data.wfrq = 0;
        for (let i = 0; i < this.props.data.swrBasket.length; i++) {
          data.wfrq += parseInt(this.props.data.swrBasket[i].quantity, 10);
        }
    } else if (this.props.serviceType === 'household_wash_and_fold') {
        data.wfhw = this.props.weight.wfhrw;
        data.wfhq = 0;
        for (let i = 0; i < this.props.data.swhBasket.length; i++) {
          data.wfhq += parseInt(this.props.data.swhBasket[i].quantity, 10);
        }
    } else if (this.props.serviceType === 'standard_wash_and_iron') {
        data.wirw = this.props.weight.wirw;
        data.wirq = 0;
        for (let i = 0; i < this.props.data.sirBasket.length; i++) {
          data.wirq += parseInt(this.props.data.sirBasket[i].quantity, 10);
        }
    } else if (this.props.serviceType === 'household_wash_and_iron') {
        data.wihw = this.props.weight.wihrw;
        data.wihq = 0;
        for (let i = 0; i < this.props.data.sihBasket.length; i++) {
          data.wihq += parseInt(this.props.data.sihBasket[i].quantity, 10);
        }
    } else if (this.props.serviceType === 'dry_cleaning') {
      data.dcq = 0;
      for (let i = 0; i < this.props.data.dcBasket.length; i++) {
        data.dcq += parseInt(this.props.data.dcBasket[i].quantity, 10);
      }
    } else if (this.props.serviceType === 'premium') {
      data.pmq = 0;
      for (let i = 0; i < this.props.data.pmBasket.length; i++) {
        data.pmq += parseInt(this.props.data.pmBasket[i].quantity, 10);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.status !== this.props.status &&
        nextProps.status === 'success') {
        Alert.alert(
              '',
              'Basket Added',
              [{ text: 'OK', onPress: () => Actions.pickup() }],
            );
      }

      if (nextProps.error !== this.props.error &&
        nextProps.error) {
        Alert.alert(
              'Error',
              nextProps.error,
              [{ text: 'OK' }],
            );
      }
  }

  onPressWF() {
    Actions.services({ service_type: 'standard_wash_and_fold', data });
  }

  onPressWFHH() {
    Actions.services({ service_type: 'household_wash_and_fold', data });
  }

  onPressWI() {
    Actions.services({ service_type: 'standard_wash_and_iron', data });
  }

  onPressWIHH() {
    Actions.services({ service_type: 'household_wash_and_iron', data });
  }

  onPressDC() {
    Actions.services({ service_type: 'dry_cleaning', data });
  }
  onPressPM() {
    Actions.services({ service_type: 'premium', data });
  }

  onSubmitBtnPress() {
    const sb = { super_baskets: [], order_id: 0 };
    const superbaskets = [];
    const swBasket = {
       service_type: 'standard_wash_and_fold',
        regular: { baskets: [],
          cost: 0,
          weight: 0,
          quantity: 0
      }
  };
    const hwBasket = {
      service_type: 'household_wash_and_fold',
        regular: { baskets: [],
          cost: 0,
          weight: 0,
          quantity: 0
      }
  };
    const siBasket = {
      service_type: 'standard_wash_and_iron',
        regular: { baskets: [],
          cost: 0,
          weight: 0,
          quantity: 0
      }
  };
    const hiBasket = {
      service_type: 'household_wash_and_iron',
        regular: { baskets: [],
          cost: 0,
          weight: 0,
          quantity: 0
      }
  };
    const dcBasket = {
       service_type: 'dry_cleaning',
       regular: { baskets: [],
         cost: 0,
         weight: 0,
         quantity: 0
       }
    };
    const pmBasket = {
       service_type: 'premium',
       regular: { baskets: [],
         cost: 0,
         weight: 0,
         quantity: 0
       }
    };

    swBasket.regular.baskets = data.swrBasket;
    const wfrw = data.wfrw ? data.wfrw : 0;
    swBasket.regular.weight = parseFloat(wfrw, 10);
    swBasket.regular.cost = swBasket.regular.weight * 60;
    swBasket.regular.id = data.swrBasket.id;
    swBasket.regular.quantity = data.wfrq;
    superbaskets.push(swBasket);


    hwBasket.regular.baskets = data.swhBasket;
    const wfhw = data.wfhw ? data.wfhw : 0;
    hwBasket.regular.weight = parseFloat(wfhw, 10);
    hwBasket.regular.cost = hwBasket.regular.weight * 95;
    hwBasket.regular.id = data.swhBasket.id;
    hwBasket.regular.quantity = data.wfhq;
    superbaskets.push(hwBasket);

    siBasket.regular.baskets = data.sirBasket;
    const wirw = data.wirw ? data.wirw : 0;
    siBasket.regular.weight = parseFloat(wirw, 10);
    siBasket.regular.cost = siBasket.regular.weight * 80;
    siBasket.regular.id = data.sirBasket.id;
    siBasket.regular.quantity = data.wirq;
    superbaskets.push(siBasket);

    hiBasket.regular.baskets = data.sihBasket;
    const wihw = data.wihw ? data.wihw : 0;
    hiBasket.regular.weight = parseFloat(wihw, 10);
    hiBasket.regular.cost = hiBasket.regular.weight * 115;
    hiBasket.regular.id = data.sihBasket.id;
    hiBasket.regular.quantity = data.wihq;
    superbaskets.push(hiBasket);

    for (let i = 0; i < data.dcBasket.length; i++) {
      dcBasket.regular.baskets.push(data.dcBasket[i]);
    }
    dcBasket.regular.cost = data.dcBasket.cost;
    dcBasket.regular.id = data.dcBasket.id;
    dcBasket.regular.quantity = data.dcq;
    superbaskets.push(dcBasket);

    for (let i = 0; i < data.pmBasket.length; i++) {
      pmBasket.regular.baskets.push(data.pmBasket[i]);
    }
    pmBasket.regular.cost = data.pmBasket.cost;
    pmBasket.regular.id = data.pmBasket.id;
    pmBasket.regular.quantity = data.pmq;
    superbaskets.push(pmBasket);

    sb.super_baskets = superbaskets;
    sb.order_id = data.order_id || this.state.data.order_id;
    if (data.state === 'edit') {
      Alert.alert(
    '',
    'Sure you want to update the Basket?',
      [
        { text: 'Cancel' },
        { text: 'Confirm',
        onPress: () => this.props.updateBasket(sb, this.props.session_token || this.state.token) }
      ]
    );
  } else {
    Alert.alert(
  '',
  'Sure you want to add the Basket?',
    [
      { text: 'Cancel' },
      { text: 'Confirm',
      onPress: () => this.props.createBasket(sb, this.props.session_token || this.state.token) }
    ]
  );
  }
}

  render() {
    if (this.props.loading) {
      return (
        <ActivityIndicator />
      );
    }
    return (
      <ScrollView style={{ flex: 1 }}>
        <Text style={styles.HeaderTextStyle}>Order ID: {data.order_id ||
          this.state.data.order_id}</Text>
        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Wash and Fold</Text>
            <CardSection>
            <Text style={styles.TextStyle}>Weight: { this.state.data.wfrw ?
              this.state.data.wfrw : 0}</Text>
            <Text style={styles.TextStyle}>Qty: { this.state.data.wfrq ? this.state.data.wfrq : 0}
            </Text>
            </CardSection>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressWF()}
          >
            ADD
          </Button>
          </View>
        </CardSection>

        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Household WF</Text>
            <CardSection>
            <Text style={styles.TextStyle}>Weight: { this.state.data.wfhw ?
              this.state.data.wfhw : 0}
            </Text>
            <Text style={styles.TextStyle}>Qty: { this.state.data.wfhq ? this.state.data.wfhq : 0}
            </Text>
            </CardSection>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressWFHH()}
          >
            ADD
          </Button>
          </View>
        </CardSection>

        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Wash and Iron</Text>
            <CardSection>
            <Text style={styles.TextStyle}>Weight: { this.state.data.wirw ?
              this.state.data.wirw : 0}
            </Text>
            <Text style={styles.TextStyle}>Qty: { this.state.data.wirq ? this.state.data.wirq : 0}
            </Text>
            </CardSection>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressWI()}
          >
            ADD
          </Button>
          </View>
        </CardSection>

        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Household WI</Text>
            <CardSection>
            <Text style={styles.TextStyle}>Weight: { this.state.data.wihw ?
              this.state.data.wihw : 0}
            </Text>
            <Text style={styles.TextStyle}>Qty: { this.state.data.wihq ? this.state.data.wihq : 0}
            </Text>
            </CardSection>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressWIHH()}
          >
            ADD
          </Button>
          </View>
        </CardSection>
        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Premium</Text>
            <Text style={styles.TextStyle}>Total Qty: { this.state.data.pmq ?
              this.state.data.pmq : 0}</Text>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressPM()}
          >
            ADD
          </Button>
          </View>
        </CardSection>
        <CardSection style={styles.CardSectionStyle}>
          <View style={styles.ServiceViewStyle}>
            <Text style={styles.BoldTextStyle}>Dry Cleaning</Text>
            <Text style={styles.TextStyle}>Total Qty: { this.state.data.dcq ?
              this.state.data.dcq : 0}
            </Text>
          </View>
          <View style={styles.ButtonViewStyle}>
          <Button
            onPress={() => this.onPressDC()}
          >
            ADD
          </Button>
          </View>
        </CardSection>
        <View style={styles.SubmitButtonViewStyle}>
        <Button
          onPress={() => this.onSubmitBtnPress()}
        >
          SUBMIT
        </Button>
        </View>
      </ScrollView>

    );
  }
}

const styles = {
  ViewStyle: {
    paddingVertical: 15,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    marginHorizontal: 5,
    marginTop: 10
  },
  CustomerSectionStyle: {
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  TextStyle: {
    fontSize: 16,
    fontWeight: '400',
    paddingHorizontal: 4
  },
  BoldTextStyle: {
    fontSize: 18,
    fontWeight: '500'
  },
  HeaderTextStyle: {
    fontSize: 18,
    fontWeight: '500',
    alignSelf: 'center',
    marginTop: 20
  },
  ButtonTextStyle: {
    alignSelf: 'center',
    color: '#1E88E5',
    fontSize: 20,
    fontWeight: '500',
  },
  ButtonViewStyle: {
    width: deviceWidth / 4,
    height: 50
  },
  SubmitButtonViewStyle: {
    height: 50
  },
  ServiceViewStyle: {
    width: deviceWidth / 2,
    marginLeft: 5
  },
  CardSectionStyle: {
    paddingVertical: 15,
    justifyContent: 'space-between'
  },

};


const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  const { status, error, loading } = state.basket;
  return { status, error, loading, session_token };
};


export default connect(mapStateToProps, { createBasket, updateBasket })(Baskets);
