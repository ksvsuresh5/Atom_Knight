import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Animated,
  Dimensions
} from 'react-native';

const deviceWidth = Dimensions.get('window').width;
const up = require('../../Assets/Menu/up.png');
const down = require('../../Assets/Menu/down.png');

class Panel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      expanded: false,
      visible: props.visible,
      animation: new Animated.Value()
    };
  }

  toggle() {
    let finalValue = 0;
    const initialValue = this.state.expanded
      ? this.state.maxHeight + this.state.minHeight
      : this.state.minHeight;
      if (this.props.rowCount >= 2) {
         finalValue = this.state.expanded
          ? this.state.minHeight
          : this.state.maxHeight + this.state.minHeight + (50 * this.props.rowCount);
      } else {
     finalValue = this.state.expanded
      ? this.state.minHeight
      : this.state.maxHeight + this.state.minHeight;
    }
    this.setState({
      expanded: !this.state.expanded
    });
    this.state.animation.setValue(initialValue);
    Animated.spring(
        this.state.animation, { toValue: finalValue }).start();
  }

  _setMaxHeight(event) {
    this.setState({ maxHeight: event.nativeEvent.layout.height });
  }

  _setMinHeight(event) {
    this.setState({ minHeight: event.nativeEvent.layout.height });
    this.state.animation.setValue(event.nativeEvent.layout.height);
  }

  render() {
    let icon = down;

    if (this.state.expanded) {
      icon = up;
    }

    if (!this.state.visible) {
      return null;
    }
    return (
      <Animated.View
        style={[
        styles.container, {
          height: this.state.animation
        }
      ]}
      >
        <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>

          <TouchableHighlight
            onPress={this.toggle.bind(this)} underlayColor="#f1f1f1"
          >
            <View style={styles.CardSectionStyle}>
            <Image style={styles.buttonImage} source={icon} />
              <Text style={styles.title}>{this.state.title}</Text>
            </View>
          </TouchableHighlight>

        </View>

        <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
          {this.props.children}
        </View>

      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    margin: 10,
    overflow: 'hidden'
  },
  titleContainer: {
    flexDirection: 'row',
  },
  title: {
    paddingVertical: 5,
    color: '#2a2f43',
    fontWeight: 'bold',
    marginLeft: 40
  },
  centretitle: {
    paddingVertical: 5,
    color: '#2a2f43',
  },
  buttonImage: {
    width: 12,
    height: 12,
    marginTop: 11,
    marginLeft: 5
  },
  body: {
    padding: 10,
    paddingTop: 0,
  },
  CardSectionStyle: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    width: deviceWidth,
    paddingRight: 40
  },
  TitleContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
export { Panel };
