import React, { Component } from 'react';
import { View, Image, ImageBackground, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { mobileChanged, passwordChanged, loginUser } from '../actions';
import { CardSection, Button } from './common';

const BG_IMG = require('../Assets/Menu/BG_LOGIN.jpg');
const LOGO_IMG = require('../Assets/Menu/logo_header.png');


class Login extends Component {

  onLoginPress() {
    const data = {
      mobile: this.props.mobile,
      password: this.props.password,
    };
    this.props.loginUser(data);
}

    render() {
        return (
          <ImageBackground
            source={BG_IMG}
            style={styles.BGImgStyle}
          >

            <View style={styles.ContainerViewStyle}>
                <View style={styles.TitleContainerStyle} >
                  <Image
                    source={LOGO_IMG}
                    style={styles.LogoImgStyle}
                  />
                </View>
                <CardSection style={styles.TextInputSectionStyle}>
                  <Text style={styles.labelStyle}>Mobile No.</Text>
                  <View style={styles.InputContainerStyle}>
                  <TextInput
                    label="Mobile"
                    keyboardType='phone-pad'
                    autoCorrect={false}
                    selectionColor='#fff'
                    style={styles.InputStyle}
                    onChangeText={value => this.props.mobileChanged(value)}
                    value={this.props.mobile}
                  />
                  </View>
                </CardSection>
                <CardSection style={styles.TextInputSectionStyle}>
                  <Text style={styles.labelStyle}>Password</Text>
                  <View style={styles.InputContainerStyle}>
                  <TextInput
                    label="Password"
                    autoCorrect={false}
                    selectionColor='#fff'
                    style={styles.InputStyle}
                    secureTextEntry
                    onChangeText={value => this.props.passwordChanged(value)}
                    value={this.props.password}
                  />
                  </View>
                </CardSection>
                <Text style={styles.errorTextStyle}>{this.props.error}</Text>
                <View style={styles.LoginButtonContainerStyle}>
                  <Button
                    onPress={() => this.onLoginPress()}
                  >
                    LOGIN
                  </Button>
                  </View>
            </View>

        </ImageBackground>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    ContainerViewStyle: {
      width: 300,
    },
    BGImgStyle: {
      flex: 1,
      width: null,
      height: null,
      backgroundColor: 'transparent',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 0,
      paddingTop: 0
    },
    LogoImgStyle: {
      width: 190,
      height: 52,
    },

    TitleContainerStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 70
    },
    InputContainerStyle: {
      marginLeft: 20,
    },
    labelStyle: {
      color: '#fff',
      fontSize: 20,
      paddingTop: 10
    },
    TextInputSectionStyle: {
    },
    InputStyle: {
      width: 200,
      fontWeight: '400',
      color: '#fff',
      fontSize: 16,
      textAlign: 'center'
    },
    LoginButtonContainerStyle: {
      height: 50,
      marginTop: 50
    },
};

const mapStateToProps = ({ auth }) => {
    const { mobile, password, error, loading } = auth;

    return { mobile, password, error, loading };
};

export default connect(mapStateToProps, { mobileChanged, passwordChanged, loginUser })(Login);
