import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { setCustomText } from 'react-native-global-props';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (

            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

const customTextProps = {
  style: {
    fontFamily: 'OpenSans',
  }
};
setCustomText(customTextProps);

export default App;
