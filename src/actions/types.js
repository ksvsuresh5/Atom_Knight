export const MOBILE_CHANGED = 'mobile_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const PIKCUP_FETCH_SUCCESS = 'pickup_fetch_success';
export const DELIVERY_FETCH_SUCCESS = 'delivery_fetch_success';
export const PICKUPCOMPLETE_FETCH_SUCCESS = 'pickupcomplete_fetch_success';
export const DELIVERYCOMPLETE_FETCH_SUCCESS = 'deliverycomplete_fetch_success';
export const SERVICE_FETCH_SUCCESS = 'service_fetch_success';

export const PICKUP_COMPLETE_SUCCESS = 'pickup_complete_success';
export const DELIVERY_COMPLETE_SUCCESS = 'delivery_complete_success';

export const COUNT_CHANGED = 'count_changed';
export const WEIGHT_CHANGED = 'weight_changed';

export const CREATE_BASKET = 'create_basket';
export const CREATE_BASKET_SUCCESS = 'create_basket_success';
export const CREATE_BASKET_FAIL = 'create_basket_fail';
export const UPDATE_BASKET = 'update_basket';
