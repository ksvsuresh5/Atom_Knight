import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import {
  MOBILE_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
} from './types';

const request = axios.create({
  headers: {
    'Content-Type': 'application/json'
  }
});

export const mobileChanged = (text) => ({
    type: MOBILE_CHANGED,
    payload: text
  });

export const passwordChanged = (text) => ({
    type: PASSWORD_CHANGED,
    payload: text
  });


export const loginUser = (data) => (dispatch) => {
        dispatch({ type: LOGIN_USER });
        const URL = 'http://thewashapp.in/api/v1/executives/auth/signin';
        request.post(URL, data)
          .then(response => loginUserSuccess(dispatch, response.data))
            .catch(() => loginUserFail(dispatch));
  };


const loginUserFail = (dispatch) => {
    dispatch({ type: LOGIN_USER_FAIL });
  };

const loginUserSuccess = (dispatch, data) => {
    dispatch({
      type: LOGIN_USER_SUCCESS,
      payload: data
    });
  AsyncStorage.setItem('token', data.session_token);
  AsyncStorage.setItem('name', data.name);
  if (data.success) {
    Actions.pickup();
  }
};
