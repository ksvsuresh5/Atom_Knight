import axios from 'axios';
import {
  SERVICE_FETCH_SUCCESS,
  COUNT_CHANGED,
  WEIGHT_CHANGED
} from './types';

export const countChanged = ({ prop, value }) => ({
  type: COUNT_CHANGED,
  payload: { prop, value }
  });

export const weightChanged = ({ prop, value }) => ({
    type: WEIGHT_CHANGED,
    payload: { prop, value }
  });

export const serviceFetch = (serviceType, sessionToken) => (dispatch) => {
    axios.get(`http://thewashapp.in/api/v1/executives/services/${serviceType}`, { headers: { 'Session-Token': sessionToken } })
      .then(response => serviceFetchSuccess(dispatch, response.data.services))
      .catch((error) => console.log(error));
  };

const serviceFetchSuccess = (dispatch, data) => {
  dispatch({
    type: SERVICE_FETCH_SUCCESS,
    payload: data
  });
};
