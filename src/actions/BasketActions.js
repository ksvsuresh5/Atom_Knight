import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {
  CREATE_BASKET,
  CREATE_BASKET_SUCCESS,
  CREATE_BASKET_FAIL,
  UPDATE_BASKET
} from './types';

const request = axios.create({
  headers: {
    'Content-Type': 'application/json'
  }
});

export const createBasket = (data, sessionToken) => (dispatch) => {
        dispatch({ type: CREATE_BASKET });
        const URL = 'http://thewashapp.in/api/v1/executives/baskets';
        const config = { headers: { 'Session-Token': sessionToken } };
        request.post(URL, data, config)
          .then(response => createBasketSuccess(dispatch, response.data))
            .catch(() => createBasketFail(dispatch));
  };

export const updateBasket = (data, sessionToken) => (dispatch) => {
        dispatch({ type: UPDATE_BASKET });
        const URL = `http://thewashapp.in/api/v1/executives/baskets/${data.order_id}`;
        const config = { headers: { 'Session-Token': sessionToken } };
        request.put(URL, data, config)
          .then(response => createBasketSuccess(dispatch, response.data))
            .catch(() => createBasketFail(dispatch));
  };

export const basketFetch = (orderID, sessionToken, state) => () => {
    axios.get(`http://thewashapp.in/api/v1/executives/baskets?order_id=${orderID}`, { headers: { 'Session-Token': sessionToken } })
      .then(response => Actions.baskets({ order_id: orderID,
        sb: response.data.superbaskets,
        state }))
      .catch((error) => console.log(error));
  };


const createBasketFail = (dispatch) => {
    dispatch({ type: CREATE_BASKET_FAIL });
  };

const createBasketSuccess = (dispatch, data) => {
    dispatch({
      type: CREATE_BASKET_SUCCESS,
      payload: data
    });
};
