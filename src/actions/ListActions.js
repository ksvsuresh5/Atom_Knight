import axios from 'axios';
import {
  PIKCUP_FETCH_SUCCESS,
  DELIVERY_FETCH_SUCCESS,
  PICKUPCOMPLETE_FETCH_SUCCESS,
  DELIVERYCOMPLETE_FETCH_SUCCESS,
  PICKUP_COMPLETE_SUCCESS,
  DELIVERY_COMPLETE_SUCCESS
} from './types';

const request = axios.create({
  headers: {
    'Content-Type': 'application/json'
  }
});

export const pickupFetch = (sessionToken) => (dispatch) => {
    axios.get('http://thewashapp.in/api/v1/executives/tasks/pickups', { headers: { 'Session-Token': sessionToken } })
      .then(response => pickupFetchSuccess(dispatch, response.data.orders))
      .catch((error) => console.log(error));
  };

export const deliveryFetch = (sessionToken) => (dispatch) => {
    axios.get('http://thewashapp.in/api/v1/executives/tasks/deliveries', { headers: { 'Session-Token': sessionToken } })
      .then(response => deliveryFetchSuccess(dispatch, response.data.orders))
      .catch((error) => console.log(error));
  };

export const pickupcompleteFetch = (sessionToken) => (dispatch) => {
    axios.get('http://thewashapp.in/api/v1/executives/tasks/pickups_completed', { headers: { 'Session-Token': sessionToken } })
      .then(response => pickupCompleteFetchSuccess(dispatch, response.data.orders))
      .catch((error) => console.log(error));
  };

export const deliverycompleteFetch = (sessionToken) => (dispatch) => {
    axios.get('http://thewashapp.in/api/v1/executives/tasks/deliveries_completed', { headers: { 'Session-Token': sessionToken } })
      .then(response => deliveryCompleteFetchSuccess(dispatch, response.data.orders))
      .catch((error) => console.log(error));
  };

export const pickupComplete = (orderID, sessionToken) => (dispatch) => {
  const URL = 'http://thewashapp.in/api/v1/executives/tasks/update_order';
  const config = { headers: { 'Session-Token': sessionToken } };
  const data = {
    order_id: orderID,
    category: 0,
    status: 2
  };

    request.post(URL, data, config)
    .then(response => pickupCompleteSuccess(dispatch, response.data))
    .catch((error) => console.log(error));
  };

export const deliveryComplete = (orderID, sessionToken) => (dispatch) => {
  const URL = 'http://thewashapp.in/api/v1/executives/tasks/update_order';
  const config = { headers: { 'Session-Token': sessionToken } };
  const data = {
      order_id: orderID,
      category: 1,
      status: 2,
    };

      request.post(URL, data, config)
      .then(response => deliveryCompleteSuccess(dispatch, response.data))
      .catch((error) => console.log(error));
    };


const pickupFetchSuccess = (dispatch, data) => {
  dispatch({
    type: PIKCUP_FETCH_SUCCESS,
    payload: data
  });
};

const deliveryFetchSuccess = (dispatch, data) => {
  dispatch({
    type: DELIVERY_FETCH_SUCCESS,
    payload: data
  });
};

const pickupCompleteFetchSuccess = (dispatch, data) => {
  dispatch({
    type: PICKUPCOMPLETE_FETCH_SUCCESS,
    payload: data
  });
};

const deliveryCompleteFetchSuccess = (dispatch, data) => {
  dispatch({
    type: DELIVERYCOMPLETE_FETCH_SUCCESS,
    payload: data
  });
};

const pickupCompleteSuccess = (dispatch, data) => {
  dispatch({
    type: PICKUP_COMPLETE_SUCCESS,
    payload: data
  });
};

const deliveryCompleteSuccess = (dispatch, data) => {
  dispatch({
    type: DELIVERY_COMPLETE_SUCCESS,
    payload: data
  });
};
