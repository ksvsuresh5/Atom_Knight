import {
  CREATE_BASKET,
  CREATE_BASKET_SUCCESS,
  CREATE_BASKET_FAIL
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  status: false,
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_BASKET:
      return { ...state, loading: true, error: '' };
    case CREATE_BASKET_SUCCESS:
      return { ...state, ...INITIAL_STATE, status: action.payload.status };
    case CREATE_BASKET_FAIL:
      return { ...state, error: 'Basket Creation Failed.', loading: false };
    default:
      return state;
  }
};
