import {
  PIKCUP_FETCH_SUCCESS,
  DELIVERY_FETCH_SUCCESS,
  PICKUPCOMPLETE_FETCH_SUCCESS,
  DELIVERYCOMPLETE_FETCH_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {

};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PIKCUP_FETCH_SUCCESS:
      return action.payload;
    case DELIVERY_FETCH_SUCCESS:
      return action.payload;
    case PICKUPCOMPLETE_FETCH_SUCCESS:
      return action.payload;
    case DELIVERYCOMPLETE_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
