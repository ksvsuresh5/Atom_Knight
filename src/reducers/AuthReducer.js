import {
  PASSWORD_CHANGED,
  MOBILE_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,

} from '../actions/types';

const INITIAL_STATE = {
  password: '',
  mobile: '',
  session_token: '',
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case MOBILE_CHANGED:
      return { ...state, mobile: action.payload };
    case LOGIN_USER:
      return { ...state, loading: true, error: '' };
    case LOGIN_USER_SUCCESS:
      return { ...state,
              ...INITIAL_STATE,
              session_token: action.payload.session_token };
    case LOGIN_USER_FAIL:
        return { ...state,
                error: 'Authentication Failed.',
                mobile: '',
                password: '',
                loading: false };
    default:
      return state;
  }
};
