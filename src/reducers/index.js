import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import ListReducer from './ListReducer';
import ServiceReducer from './ServiceReducer';
import BasketReducer from './BasketReducer';
import QtyReducer from './QtyReducer';
import WeightReducer from './WeightReducer';
import StatusReducer from './StatusReducer';

export default combineReducers({
  auth: AuthReducer,
  list: ListReducer,
  services: ServiceReducer,
  basket: BasketReducer,
  item: QtyReducer,
  weight: WeightReducer,
  status: StatusReducer
});
