import {
  PICKUP_COMPLETE_SUCCESS,
  DELIVERY_COMPLETE_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  p_status: '',
  d_status: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PICKUP_COMPLETE_SUCCESS:
      return { ...state, ...INITIAL_STATE, p_status: action.payload.success };
    case DELIVERY_COMPLETE_SUCCESS:
      return { ...state, ...INITIAL_STATE, d_status: action.payload.success };
    default:
      return state;
  }
};
